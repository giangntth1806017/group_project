create table MANAGER (
MANAGER_ID int identity not null,
NAME nvarchar(50),
USERNAME nvarchar(50),
PASS nvarchar(50),
EMAIL nvarchar(50),
PHONE nvarchar(50),
MANAGER_STATUS nvarchar(50),
primary key (MANAGER_ID)
);

create table CANDIDATE (
CANDIDATE_ID int identity not null,
EXAM_ID int,
CANDIDATE nvarchar(255),
FIRST_NAME nvarchar(100),
LAST_NAME nvarchar(100),
DOB datetime,
IDENTITY_ID nvarchar(50),
JOB nvarchar(250),
EDUCATION nvarchar(250),
USERNAME nvarchar(50),
PASS nvarchar(50),
EMAIL nvarchar(50),
PHONE nvarchar(50),
CANDIDATE_STATUS nvarchar(50),
primary key (CANDIDATE_ID)
);

create table RESULT (
RESULT_ID int identity not null,
RESULT nvarchar(50),
CANDIDATE_ID int,
EXAM_ID int,
SCORE int,
RESULT_STATUS nvarchar(50),
primary key (RESULT_ID)
);

create table EXAM (
EXAM_ID int identity not null,
NAME nvarchar(250),
START_DAY datetime,
END_DAY datetime,
DURATION int,
primary key (EXAM_ID)
);

create table EXAM_SUBJECT (
SUBJECT_ID int identity not null,
NAME nvarchar(250),
SUBJECT_DESCRIPTION nvarchar(250),
primary key (SUBJECT_ID)
);

create table QUESTION (
QUESTION_ID int identity not null,
SUBJECT_ID int,
QUESTION_TEXT nvarchar(250),
Point int,
primary key (QUESTION_ID)
);

create table Answer(
AnswerID int identity not null,
primary key (AnswerID),
AnswerText nvarchar(255),
QUESTION_ID int
);

create table Choice(
ChoiceID int identity not null,
primary key (ChoiceID),
ChoiceText nvarchar(255),
isAnswer bit,
isSelected bit,
QUESTION_ID int
);

alter table CANDIDATE
add constraint CANDIDATE_EXAM_FK
foreign key (EXAM_ID)
references EXAM;

alter table RESULT
add constraint RESULT_CANDIDATE_FK
foreign key (CANDIDATE_ID)
references CANDIDATE;

alter table RESULT
add constraint RESULT_EXAM_FK
foreign key (EXAM_ID)
references EXAM;

alter table QUESTION
add constraint QUESTION_SUBJECT_FK
foreign key (SUBJECT_ID)
references EXAM_SUBJECT;

alter table Answer
add constraint Answer_Question_Fk
foreign key (QUESTION_ID)
references QUESTION;

alter table Choice
add constraint Choice_Question_Fk
foreign key (QUESTION_ID)
references QUESTION;
