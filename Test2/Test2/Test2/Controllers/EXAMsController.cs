﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Test2.Models;

namespace Test2.Controllers
{
    public class EXAMsController : Controller
    {
        private T1808M_NguyenTruongGiangEntities db = new T1808M_NguyenTruongGiangEntities();

        // GET: EXAMs
        public ActionResult Index()
        {
            return View(db.EXAMs.ToList());
        }

        // GET: EXAMs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EXAM eXAM = db.EXAMs.Find(id);
            if (eXAM == null)
            {
                return HttpNotFound();
            }
            return View(eXAM);
        }

        // GET: EXAMs/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: EXAMs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "EXAM_ID,NAME,START_DAY,END_DAY,DURATION")] EXAM eXAM)
        {
            if (ModelState.IsValid)
            {
                db.EXAMs.Add(eXAM);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(eXAM);
        }

        // GET: EXAMs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EXAM eXAM = db.EXAMs.Find(id);
            if (eXAM == null)
            {
                return HttpNotFound();
            }
            return View(eXAM);
        }

        // POST: EXAMs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "EXAM_ID,NAME,START_DAY,END_DAY,DURATION")] EXAM eXAM)
        {
            if (ModelState.IsValid)
            {
                db.Entry(eXAM).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(eXAM);
        }

        // GET: EXAMs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EXAM eXAM = db.EXAMs.Find(id);
            if (eXAM == null)
            {
                return HttpNotFound();
            }
            return View(eXAM);
        }

        // POST: EXAMs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            EXAM eXAM = db.EXAMs.Find(id);
            db.EXAMs.Remove(eXAM);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
