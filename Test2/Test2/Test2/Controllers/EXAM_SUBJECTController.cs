﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Test2.Models;

namespace Test2.Controllers
{
    public class EXAM_SUBJECTController : Controller
    {
        private T1808M_NguyenTruongGiangEntities db = new T1808M_NguyenTruongGiangEntities();

        // GET: EXAM_SUBJECT
        public ActionResult Index()
        {
            return View(db.EXAM_SUBJECT.ToList());
        }

        // GET: EXAM_SUBJECT/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EXAM_SUBJECT eXAM_SUBJECT = db.EXAM_SUBJECT.Find(id);
            if (eXAM_SUBJECT == null)
            {
                return HttpNotFound();
            }
            return View(eXAM_SUBJECT);
        }

        // GET: EXAM_SUBJECT/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: EXAM_SUBJECT/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "SUBJECT_ID,NAME,SUBJECT_DESCRIPTION")] EXAM_SUBJECT eXAM_SUBJECT)
        {
            if (ModelState.IsValid)
            {
                db.EXAM_SUBJECT.Add(eXAM_SUBJECT);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(eXAM_SUBJECT);
        }

        // GET: EXAM_SUBJECT/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EXAM_SUBJECT eXAM_SUBJECT = db.EXAM_SUBJECT.Find(id);
            if (eXAM_SUBJECT == null)
            {
                return HttpNotFound();
            }
            return View(eXAM_SUBJECT);
        }

        // POST: EXAM_SUBJECT/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "SUBJECT_ID,NAME,SUBJECT_DESCRIPTION")] EXAM_SUBJECT eXAM_SUBJECT)
        {
            if (ModelState.IsValid)
            {
                db.Entry(eXAM_SUBJECT).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(eXAM_SUBJECT);
        }

        // GET: EXAM_SUBJECT/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EXAM_SUBJECT eXAM_SUBJECT = db.EXAM_SUBJECT.Find(id);
            if (eXAM_SUBJECT == null)
            {
                return HttpNotFound();
            }
            return View(eXAM_SUBJECT);
        }

        // POST: EXAM_SUBJECT/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            EXAM_SUBJECT eXAM_SUBJECT = db.EXAM_SUBJECT.Find(id);
            db.EXAM_SUBJECT.Remove(eXAM_SUBJECT);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
