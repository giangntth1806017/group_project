﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Test2.Models;

namespace Test2.Controllers
{
    public class QUESTIONsController : Controller
    {
        private T1808M_NguyenTruongGiangEntities db = new T1808M_NguyenTruongGiangEntities();

        // GET: QUESTIONs
        public ActionResult Index()
        {
            var qUESTIONs = db.QUESTIONs.Include(q => q.EXAM_SUBJECT1);
            return View(qUESTIONs.ToList());
        }

        // GET: QUESTIONs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            QUESTION qUESTION = db.QUESTIONs.Find(id);
            if (qUESTION == null)
            {
                return HttpNotFound();
            }
            return View(qUESTION);
        }

        // GET: QUESTIONs/Create
        public ActionResult Create()
        {
            ViewBag.SUBJECT_ID = new SelectList(db.EXAM_SUBJECT, "SUBJECT_ID", "NAME");
            return View();
        }

        // POST: QUESTIONs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "QUESTION_ID,QUESTION_TEXT,Point,SUBJECT_ID")] QUESTION qUESTION)
        {
            if (ModelState.IsValid)
            {
                db.QUESTIONs.Add(qUESTION);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.SUBJECT_ID = new SelectList(db.EXAM_SUBJECT, "SUBJECT_ID", "NAME", qUESTION.SUBJECT_ID);
            return View(qUESTION);
        }

        // GET: QUESTIONs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            QUESTION qUESTION = db.QUESTIONs.Find(id);
            if (qUESTION == null)
            {
                return HttpNotFound();
            }
            ViewBag.SUBJECT_ID = new SelectList(db.EXAM_SUBJECT, "SUBJECT_ID", "NAME", qUESTION.SUBJECT_ID);
            return View(qUESTION);
        }

        // POST: QUESTIONs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "QUESTION_ID,QUESTION_TEXT,Point,SUBJECT_ID")] QUESTION qUESTION)
        {
            if (ModelState.IsValid)
            {
                db.Entry(qUESTION).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.SUBJECT_ID = new SelectList(db.EXAM_SUBJECT, "SUBJECT_ID", "NAME", qUESTION.SUBJECT_ID);
            return View(qUESTION);
        }

        // GET: QUESTIONs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            QUESTION qUESTION = db.QUESTIONs.Find(id);
            if (qUESTION == null)
            {
                return HttpNotFound();
            }
            return View(qUESTION);
        }

        // POST: QUESTIONs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            QUESTION qUESTION = db.QUESTIONs.Find(id);
            db.QUESTIONs.Remove(qUESTION);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
