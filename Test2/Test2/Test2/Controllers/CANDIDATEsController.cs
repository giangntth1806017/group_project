﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Test2.Models;

namespace Test2.Controllers
{
    public class CANDIDATEsController : Controller
    {
        private T1808M_NguyenTruongGiangEntities db = new T1808M_NguyenTruongGiangEntities();

        // GET: CANDIDATEs
        public ActionResult Index()
        {
            var cANDIDATEs = db.CANDIDATEs.Include(c => c.EXAM);
            return View(cANDIDATEs.ToList());
        }

        // GET: CANDIDATEs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CANDIDATE cANDIDATE = db.CANDIDATEs.Find(id);
            if (cANDIDATE == null)
            {
                return HttpNotFound();
            }
            return View(cANDIDATE);
        }

        // GET: CANDIDATEs/Create
        public ActionResult Create()
        {
            ViewBag.EXAM_ID = new SelectList(db.EXAMs, "EXAM_ID", "NAME");
            return View();
        }

        // POST: CANDIDATEs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CANDIDATE_ID,EXAM_ID,CANDIDATE1,FIRST_NAME,LAST_NAME,DOB,IDENTITY_ID,JOB,EDUCATION,USERNAME,PASS,EMAIL,PHONE,CANDIDATE_STATUS")] CANDIDATE cANDIDATE)
        {
            if (ModelState.IsValid)
            {
                db.CANDIDATEs.Add(cANDIDATE);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.EXAM_ID = new SelectList(db.EXAMs, "EXAM_ID", "NAME", cANDIDATE.EXAM_ID);
            return View(cANDIDATE);
        }

        // GET: CANDIDATEs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CANDIDATE cANDIDATE = db.CANDIDATEs.Find(id);
            if (cANDIDATE == null)
            {
                return HttpNotFound();
            }
            ViewBag.EXAM_ID = new SelectList(db.EXAMs, "EXAM_ID", "NAME", cANDIDATE.EXAM_ID);
            return View(cANDIDATE);
        }

        // POST: CANDIDATEs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CANDIDATE_ID,EXAM_ID,CANDIDATE1,FIRST_NAME,LAST_NAME,DOB,IDENTITY_ID,JOB,EDUCATION,USERNAME,PASS,EMAIL,PHONE,CANDIDATE_STATUS")] CANDIDATE cANDIDATE)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cANDIDATE).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.EXAM_ID = new SelectList(db.EXAMs, "EXAM_ID", "NAME", cANDIDATE.EXAM_ID);
            return View(cANDIDATE);
        }

        // GET: CANDIDATEs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CANDIDATE cANDIDATE = db.CANDIDATEs.Find(id);
            if (cANDIDATE == null)
            {
                return HttpNotFound();
            }
            return View(cANDIDATE);
        }

        // POST: CANDIDATEs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CANDIDATE cANDIDATE = db.CANDIDATEs.Find(id);
            db.CANDIDATEs.Remove(cANDIDATE);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
