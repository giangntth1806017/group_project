﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Test2.Models;
using Test2.viewModels;

namespace Test2.Controllers
{
    public class QuizzController : Controller
    {
        public T1808M_NguyenTruongGiangEntities dbContext = new T1808M_NguyenTruongGiangEntities();

        // GET: Quizz
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult GetUser()
        {
            return View();
        }

        [HttpPost]
        public ActionResult GetUser(UserVM user)
        {
            UserVM userConnected = dbContext.CANDIDATEs.Where(u => u.USERNAME == user.Username && u.PASS == user.Pass)
                                         .Select(u => new UserVM
                                         {
                                             UserID = u.CANDIDATE_ID,
                                             Username = u.USERNAME,
                                             Pass = u.PASS,

                                         }).FirstOrDefault();

            UserVM wrongPass = dbContext.CANDIDATEs.Where(u => u.USERNAME == user.Username && u.PASS != user.Pass)
                                         .Select(u => new UserVM
                                         {
                                             UserID = u.CANDIDATE_ID,
                                             Username = u.USERNAME,
                                             Pass = u.PASS,

                                         }).FirstOrDefault();

            if (userConnected != null)
            {
                Session["UserConnected"] = userConnected;
                return RedirectToAction("SelectQuizz");
            }
            else
            {
                ViewBag.Msg = "Error : User is not found !!";
                if (wrongPass != null) { ViewBag.Msg = "Wrong Password"; }
                return View();
            }
        }

        [HttpGet]
        public ActionResult SelectQuizz()
        {
            QuizVM quiz = new viewModels.QuizVM();
            quiz.ListOfQuizz = dbContext.EXAM_SUBJECT.Select(q => new SelectListItem
            {
                Text = q.NAME,
                Value = q.SUBJECT_ID.ToString()

            }).ToList();

            return View(quiz);
        }

        [HttpPost]
        public ActionResult SelectQuizz(QuizVM quiz)
        {
            QuizVM quizSelected = dbContext.EXAM_SUBJECT.Where(q => q.SUBJECT_ID == quiz.QuizID).Select(q => new QuizVM
            {
                QuizID = q.SUBJECT_ID,
                QuizName = q.NAME,

            }).FirstOrDefault();

            if (quizSelected != null)
            {
                Session["SelectedQuiz"] = quizSelected;

                return RedirectToAction("QuizTest");
            }

            return View();
        }

        [HttpGet]
        public ActionResult QuizTest()
        {
            QuizVM quizSelected = Session["SelectedQuiz"] as QuizVM;
            IQueryable<QuestionVM> questions = null;

            if (quizSelected != null)
            {
                questions = dbContext.QUESTIONs.Where(q => q.SUBJECT_ID == quizSelected.QuizID)
                   .Select(q => new QuestionVM
                   {
                       QuestionID = q.QUESTION_ID,
                       QuestionText = q.QUESTION_TEXT,
                       Choices = q.Choices.Select(c => new ChoiceVM
                       {
                           ChoiceID = c.ChoiceID,
                           ChoiceText = c.ChoiceText
                       }).ToList()

                   }).AsQueryable();


            }
            return View(questions);
        }

        [HttpPost]
        public ActionResult QuizTest(List<QuizAnswersVM> resultQuiz)
        {
            List<QuizAnswersVM> finalResultQuiz = new List<viewModels.QuizAnswersVM>();

            foreach (QuizAnswersVM answser in resultQuiz)
            {
                QuizAnswersVM result = dbContext.Answers.Where(a => a.QUESTION_ID == answser.QuestionID).Select(a => new QuizAnswersVM
                {
                    QuestionID = a.QUESTION_ID.Value,
                    AnswerQ = a.AnswerText,
                    isCorrect = (answser.AnswerQ.ToLower().Equals(a.AnswerText.ToLower()))

                }).FirstOrDefault();

                finalResultQuiz.Add(result);
            }

            return Json(new { result = finalResultQuiz }, JsonRequestBehavior.AllowGet);
        }
    }
}