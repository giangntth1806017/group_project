﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using OnlineBusTicket.Models;

namespace OnlineBusTicket.Controllers
{
    public class Ticket_OrderController : Controller
    {
        private T1808M_Nhom5_BusTicket_HomeWorkEntities db = new T1808M_Nhom5_BusTicket_HomeWorkEntities();

        // GET: Ticket_Order
        public ActionResult Index()
        {
            var ticket_Order = db.Ticket_Order.Include(t => t.Customer_Order).Include(t => t.Ticket);
            return View(ticket_Order.ToList());
        }

        // GET: Ticket_Order/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ticket_Order ticket_Order = db.Ticket_Order.Find(id);
            if (ticket_Order == null)
            {
                return HttpNotFound();
            }
            return View(ticket_Order);
        }

        // GET: Ticket_Order/Create
        public ActionResult Create()
        {
            ViewBag.Customer_Order_Id = new SelectList(db.Customer_Order, "Customer_Order_Id", "Customer_Order_Id");
            ViewBag.Ticket_Id = new SelectList(db.Tickets, "Ticket_Id", "Ticket_Id");
            return View();
        }

        // POST: Ticket_Order/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Ticket_Order_Id,Customer_Order_Id,Ticket_Id")] Ticket_Order ticket_Order)
        {
            if (ModelState.IsValid)
            {
                db.Ticket_Order.Add(ticket_Order);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Customer_Order_Id = new SelectList(db.Customer_Order, "Customer_Order_Id", "Customer_Order_Id", ticket_Order.Customer_Order_Id);
            ViewBag.Ticket_Id = new SelectList(db.Tickets, "Ticket_Id", "Ticket_Id", ticket_Order.Ticket_Id);
            return View(ticket_Order);
        }

        // GET: Ticket_Order/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ticket_Order ticket_Order = db.Ticket_Order.Find(id);
            if (ticket_Order == null)
            {
                return HttpNotFound();
            }
            ViewBag.Customer_Order_Id = new SelectList(db.Customer_Order, "Customer_Order_Id", "Customer_Order_Id", ticket_Order.Customer_Order_Id);
            ViewBag.Ticket_Id = new SelectList(db.Tickets, "Ticket_Id", "Ticket_Id", ticket_Order.Ticket_Id);
            return View(ticket_Order);
        }

        // POST: Ticket_Order/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Ticket_Order_Id,Customer_Order_Id,Ticket_Id")] Ticket_Order ticket_Order)
        {
            if (ModelState.IsValid)
            {
                db.Entry(ticket_Order).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Customer_Order_Id = new SelectList(db.Customer_Order, "Customer_Order_Id", "Customer_Order_Id", ticket_Order.Customer_Order_Id);
            ViewBag.Ticket_Id = new SelectList(db.Tickets, "Ticket_Id", "Ticket_Id", ticket_Order.Ticket_Id);
            return View(ticket_Order);
        }

        // GET: Ticket_Order/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ticket_Order ticket_Order = db.Ticket_Order.Find(id);
            if (ticket_Order == null)
            {
                return HttpNotFound();
            }
            return View(ticket_Order);
        }

        // POST: Ticket_Order/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Ticket_Order ticket_Order = db.Ticket_Order.Find(id);
            db.Ticket_Order.Remove(ticket_Order);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
