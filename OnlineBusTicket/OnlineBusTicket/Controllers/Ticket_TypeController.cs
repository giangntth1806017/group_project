﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using OnlineBusTicket.Models;

namespace OnlineBusTicket.Controllers
{
    public class Ticket_TypeController : Controller
    {
        private T1808M_Nhom5_BusTicket_HomeWorkEntities db = new T1808M_Nhom5_BusTicket_HomeWorkEntities();

        // GET: Ticket_Type
        public ActionResult Index()
        {
            var ticket_Type = db.Ticket_Type.Include(t => t.Bus).Include(t => t.Ticket_Status);
            return View(ticket_Type.ToList());
        }

        // GET: Ticket_Type/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ticket_Type ticket_Type = db.Ticket_Type.Find(id);
            if (ticket_Type == null)
            {
                return HttpNotFound();
            }
            return View(ticket_Type);
        }

        // GET: Ticket_Type/Create
        public ActionResult Create()
        {
            ViewBag.Bus_Id = new SelectList(db.Buses, "Bus_Id", "Bus_Name");
            ViewBag.Status_Id = new SelectList(db.Ticket_Status, "Status_Id", "Status_Name");
            return View();
        }

        // POST: Ticket_Type/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Ticket_Type_Id,Bus_Id,Ticket_Type_Desc,Start_Day,End_Day,Quantity,Status_Id,Price")] Ticket_Type ticket_Type)
        {
            if (ModelState.IsValid)
            {
                db.Ticket_Type.Add(ticket_Type);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Bus_Id = new SelectList(db.Buses, "Bus_Id", "Bus_Name", ticket_Type.Bus_Id);
            ViewBag.Status_Id = new SelectList(db.Ticket_Status, "Status_Id", "Status_Name", ticket_Type.Status_Id);
            return View(ticket_Type);
        }

        // GET: Ticket_Type/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ticket_Type ticket_Type = db.Ticket_Type.Find(id);
            if (ticket_Type == null)
            {
                return HttpNotFound();
            }
            ViewBag.Bus_Id = new SelectList(db.Buses, "Bus_Id", "Bus_Name", ticket_Type.Bus_Id);
            ViewBag.Status_Id = new SelectList(db.Ticket_Status, "Status_Id", "Status_Name", ticket_Type.Status_Id);
            return View(ticket_Type);
        }

        // POST: Ticket_Type/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Ticket_Type_Id,Bus_Id,Ticket_Type_Desc,Start_Day,End_Day,Quantity,Status_Id,Price")] Ticket_Type ticket_Type)
        {
            if (ModelState.IsValid)
            {
                db.Entry(ticket_Type).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Bus_Id = new SelectList(db.Buses, "Bus_Id", "Bus_Name", ticket_Type.Bus_Id);
            ViewBag.Status_Id = new SelectList(db.Ticket_Status, "Status_Id", "Status_Name", ticket_Type.Status_Id);
            return View(ticket_Type);
        }

        // GET: Ticket_Type/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ticket_Type ticket_Type = db.Ticket_Type.Find(id);
            if (ticket_Type == null)
            {
                return HttpNotFound();
            }
            return View(ticket_Type);
        }

        // POST: Ticket_Type/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Ticket_Type ticket_Type = db.Ticket_Type.Find(id);
            db.Ticket_Type.Remove(ticket_Type);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
