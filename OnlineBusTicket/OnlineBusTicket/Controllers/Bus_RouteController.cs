﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using OnlineBusTicket.Models;

namespace OnlineBusTicket.Controllers
{
    public class Bus_RouteController : Controller
    {
        private T1808M_Nhom5_BusTicket_HomeWorkEntities db = new T1808M_Nhom5_BusTicket_HomeWorkEntities();

        // GET: Bus_Route
        public ActionResult Index()
        {
            return View(db.Bus_Route.ToList());
        }

        // GET: Bus_Route/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Bus_Route bus_Route = db.Bus_Route.Find(id);
            if (bus_Route == null)
            {
                return HttpNotFound();
            }
            return View(bus_Route);
        }

        // GET: Bus_Route/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Bus_Route/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Bus_Route_Id,Bus_Route_Name,Bus_Route_Desc")] Bus_Route bus_Route)
        {
            if (ModelState.IsValid)
            {
                db.Bus_Route.Add(bus_Route);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(bus_Route);
        }

        // GET: Bus_Route/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Bus_Route bus_Route = db.Bus_Route.Find(id);
            if (bus_Route == null)
            {
                return HttpNotFound();
            }
            return View(bus_Route);
        }

        // POST: Bus_Route/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Bus_Route_Id,Bus_Route_Name,Bus_Route_Desc")] Bus_Route bus_Route)
        {
            if (ModelState.IsValid)
            {
                db.Entry(bus_Route).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(bus_Route);
        }

        // GET: Bus_Route/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Bus_Route bus_Route = db.Bus_Route.Find(id);
            if (bus_Route == null)
            {
                return HttpNotFound();
            }
            return View(bus_Route);
        }

        // POST: Bus_Route/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Bus_Route bus_Route = db.Bus_Route.Find(id);
            db.Bus_Route.Remove(bus_Route);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
