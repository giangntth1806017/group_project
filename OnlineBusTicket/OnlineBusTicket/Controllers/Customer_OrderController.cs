﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using OnlineBusTicket.Models;

namespace OnlineBusTicket.Controllers
{
    public class Customer_OrderController : Controller
    {
        private T1808M_Nhom5_BusTicket_HomeWorkEntities db = new T1808M_Nhom5_BusTicket_HomeWorkEntities();

        // GET: Customer_Order
        public ActionResult Index()
        {
            var customer_Order = db.Customer_Order.Include(c => c.Customer).Include(c => c.Ticket);
            return View(customer_Order.ToList());
        }

        // GET: Customer_Order/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Customer_Order customer_Order = db.Customer_Order.Find(id);
            if (customer_Order == null)
            {
                return HttpNotFound();
            }
            return View(customer_Order);
        }

        // GET: Customer_Order/Create
        public ActionResult Create()
        {
            ViewBag.Cust_Id = new SelectList(db.Customers, "Cust_Id", "Cust_Name");
            ViewBag.Ticket_Id = new SelectList(db.Tickets, "Ticket_Id", "Ticket_Id");
            return View();
        }

        // POST: Customer_Order/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Customer_Order_Id,Cust_Id,Ticket_Id,Order_Date,Price")] Customer_Order customer_Order)
        {
            if (ModelState.IsValid)
            {
                db.Customer_Order.Add(customer_Order);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Cust_Id = new SelectList(db.Customers, "Cust_Id", "Cust_Name", customer_Order.Cust_Id);
            ViewBag.Ticket_Id = new SelectList(db.Tickets, "Ticket_Id", "Ticket_Id", customer_Order.Ticket_Id);
            return View(customer_Order);
        }

        // GET: Customer_Order/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Customer_Order customer_Order = db.Customer_Order.Find(id);
            if (customer_Order == null)
            {
                return HttpNotFound();
            }
            ViewBag.Cust_Id = new SelectList(db.Customers, "Cust_Id", "Cust_Name", customer_Order.Cust_Id);
            ViewBag.Ticket_Id = new SelectList(db.Tickets, "Ticket_Id", "Ticket_Id", customer_Order.Ticket_Id);
            return View(customer_Order);
        }

        // POST: Customer_Order/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Customer_Order_Id,Cust_Id,Ticket_Id,Order_Date,Price")] Customer_Order customer_Order)
        {
            if (ModelState.IsValid)
            {
                db.Entry(customer_Order).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Cust_Id = new SelectList(db.Customers, "Cust_Id", "Cust_Name", customer_Order.Cust_Id);
            ViewBag.Ticket_Id = new SelectList(db.Tickets, "Ticket_Id", "Ticket_Id", customer_Order.Ticket_Id);
            return View(customer_Order);
        }

        // GET: Customer_Order/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Customer_Order customer_Order = db.Customer_Order.Find(id);
            if (customer_Order == null)
            {
                return HttpNotFound();
            }
            return View(customer_Order);
        }

        // POST: Customer_Order/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Customer_Order customer_Order = db.Customer_Order.Find(id);
            db.Customer_Order.Remove(customer_Order);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
