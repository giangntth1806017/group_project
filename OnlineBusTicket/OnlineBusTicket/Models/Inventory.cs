﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace OnlineBusTicket.Models
{
    public class Inventory
    {
        [Key]
        public int RecordId { get; set; }
        public string InventoryId { get; set; }
        public int TicketId { get; set; }
        public int Count { get; set; }
        public System.DateTime DateCreated { get; set; }
        public virtual Ticket Ticket { get; set; }
    }
}