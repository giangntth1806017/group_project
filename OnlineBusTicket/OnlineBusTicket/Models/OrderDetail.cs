﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OnlineBusTicket.Models
{
    public class OrderDetail
    {
        public int OrderDetailId { get; set; }
        public int CustomerOrderId { get; set; }
        public int TicketId { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }
        public virtual Ticket Ticket { get; set; }
        public virtual Ticket_Order Ticket_Order { get; set; }
        public virtual Customer_Order Customer_Order { get; set; }
    }
}